## recréer la base de donnée :

- ouvrir vs code, dans vs ouvrir le dossier du projet.
- dans un terminal sur vs code, se placer dans le même dossier
- commandes à exécuter :
  - composer install
  - npm install
  - symfony console doctrine:database:drop --force
  - symfony console doctrine:database:create
  - symfony console doctrine:migrations:migrate
  - symfony console doctrine:fixtures:load
  - composer require symfony/webpack-encore-bundle
  - npm install @symfony/webpack-encore --save-dev
  - npm install sass-loader@^9.0.1 node-sass --save-dev
  - npm uninstall node-sass
  - npm install node-sass@4.14.1
  - npm run dev-server
 
Appelez moi si y a un problème, c'est que symfony ou composer

decommenter ligne sassloader dans webpack.config.js
dans assets/styles/ changer app.css par app.scss
changer l'import dans styles/app.js

dans twig.html
{{ encore_entry_link_tags('app') }}
en dessous du premier lien bootstrap
{{ encore_entry_script_tags('app') }}
en dessous des seconds lien bootstrap