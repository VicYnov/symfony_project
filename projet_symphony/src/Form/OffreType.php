<?php

namespace App\Form;

use App\Entity\Offre;
use App\Entity\Contrat;
use App\Entity\ContratType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class OffreType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('description', TextareaType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('adresse', TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('postal_code', TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('ville', TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('end_mission_date', DateType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('contratType', EntityType::class, [
                'class' => ContratType::class,
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('contrat', EntityType::class, [
                'class' => Contrat::class,
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add("Valider", SubmitType::class, [
                "attr" => [
                    "class" => "btn btn-primary"
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offre::class,
        ]);
    }
}
