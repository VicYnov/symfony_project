<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'Username',
                TextType::class,
                [
                    "attr" => [
                        "class" => "form-control",
                        "placeholder" => "Nom d'utlilisateur"
                    ]
                ]
            )
            ->add('firstName', TextType::class, [
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "Prénom"
                ]
            ])
            ->add('lastName', TextType::class, [
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "Nom de famille"
                ]
            ])
            ->add('email', TextType::class, [
                "attr" => [
                    "class" => "form-control",
                    "placeholder" => "adresse email..."
                ]
            ])
            ->add('password', PasswordType::class, [
                "attr" => [
                    "class" => "form-control",
                ]
            ])
            ->add('confirm_passwd', PasswordType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
