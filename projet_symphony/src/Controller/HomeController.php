<?php

namespace App\Controller;

use App\Entity\Offre;
use App\Form\OffreType;
use App\Repository\OffreRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(OffreRepository $ripo)
    {
        $offres = $ripo->findAll();

        return $this->render('home/index.html.twig', [
            'offres' => $offres
        ]);
    }

    /**
     * @Route("/offre/add", name="add_offre")
     */
    public function add(OffreRepository $ripo, Request $request, EntityManagerInterface $em)
    {
        $offres = $ripo->findAll();
        $offreObject = new Offre();

        $form = $this->createForm(OffreType::class, $offreObject);

        $offreObject->setCreationDate(new DateTime());
        $offreObject->setUpdateDate(new DateTime());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($offreObject);
            $em->flush();
            $this->addFlash(
                'success',
                'Ton offre est créée!'
            );
            return $this->redirectToRoute('home');
        }

        return $this->render('home/add_offre.html.twig', [
            'offres' => $offres,
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/offre/{id}", name="show_offre")
     */
    public function show(Offre $offre)
    {
        return $this->render('home/offre.html.twig', [
            'offre' => $offre,
        ]);
    }

    /**
     * @Route("/offre/{id}/update", name="update_offre")
     */
    public function update(OffreRepository $ripo, Request $request, EntityManagerInterface $em, Offre $offre)
    {
        //$currentOffre = $ripo->find($offre);
        //$offreObject = new Offre($currentOffre);

        $form = $this->createForm(OffreType::class, $offre);

        $offre->setUpdateDate(new DateTime());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$em->persist($offreObject);
            $em->flush();
            $this->addFlash(
                'success',
                'Ton offre est modifiée!'
            );
            return $this->redirectToRoute('home');
        }

        return $this->render('home/update_offre.html.twig', [
            'offre' => $offre,
            "form" => $form->createView()
        ]);
    }

    /**
     * @Route("/offre/{id}/delete", name="delete_offre")
     */
    public function delete(Offre $offre, EntityManagerInterface $em): RedirectResponse
    {
        $em->remove($offre);
        $em->flush();
        return $this->redirectToRoute('home');
    }
}
