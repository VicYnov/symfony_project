<?php

namespace App\DataFixtures;

use App\Entity\Offre;
use App\Entity\Contrat;
use App\Entity\ContratType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');

        $contrat = new Contrat();
        $contrat->setTitle('CDD');

        $contrat2 = new Contrat();
        $contrat2->setTitle('CDI');

        $contrat3 = new Contrat();
        $contrat3->setTitle('free');

        $contratType = new ContratType();
        $contratType->setTitle('temps plein');

        $contratType2 = new ContratType();
        $contratType2->setTitle('temps partiel');

        $manager->persist($contrat);
        $manager->persist($contrat2);
        $manager->persist($contrat3);
        $manager->persist($contratType);
        $manager->persist($contratType2);


        for ($i = 0; $i < 10; $i++) {

            $offre = new Offre();
            $offre->setTitle($faker->sentence($nbWords = 2, $variableNbWords = true))
                ->setDescription($faker->sentence($nbWords = 12, $variableNbWords = true))
                ->setAdresse($faker->streetAddress())
                ->setPostalCode(intVal($faker->postcode()))
                ->setVille($faker->city())
                ->setCreationDate($faker->dateTimeBetween('-6 months', 'now'))
                ->setUpdateDate($faker->dateTimeBetween('now', '+1 days'))
                ->setEndMissionDate($faker->dateTimeBetween('now', '+6 months'))
                ->setContrat($faker->randomElement($array = array($contrat, $contrat2, $contrat3)))
                ->setContratType($faker->randomElement($array = array($contratType, $contratType2)));

            $manager->persist($offre);
        }

        $manager->flush();
    }
}
